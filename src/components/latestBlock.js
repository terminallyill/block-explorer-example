import {useState, useEffect} from 'react';

export default function GetLatestBlock({setLatestBlock, lb, setDetailBlock, db, alchemy}){

    useEffect(() => {
      async function getBlockNumber() {
        setLatestBlock(await alchemy.core.getBlockNumber());
      }
  
      setInterval(() =>{getBlockNumber()}, 5000);
      if(lb){
        console.log("Latest block number:", lb);
      } else {
        console.log("Waiting for latest block ..")
      }
    });

    function getDetails(){
        console.log("Setting detail block as:", lb);
        setDetailBlock(lb);
    }

    return (
        <>
            <div className="latest-block-container">
                {lb ? (
                    <>
                        <span>Latest Block Number: {lb}</span>
                        <button onClick={getDetails}>View Details</button>
                    </>
                )
                :
                    <>
                        <span>Loading latest block..</span>
                    </>
                }
            </div>        
        </>
    )

}