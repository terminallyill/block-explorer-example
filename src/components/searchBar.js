import React, {useState} from 'react';
import { Alchemy } from 'alchemy-sdk';

export default function BlockSearch({setSearchBlock, sb, setDetailBlock}) {
    
    function searchForBlock(e){
        setSearchBlock(e.target.value);
    }

    function getDetails(){
        console.log("Setting detail block:", sb);
        setDetailBlock(parseInt(sb));
    }
    return (
        <>  
            <div className="search-block-container">
                <input className="search-input" placeholder='Search a block number' onInput={(e) => searchForBlock(e)} value={sb}></input>
                <button onClick={getDetails}>Get Block Details</button>
            </div>
        </>
    )
}