import {useState, useEffect} from 'react';
import {Utils} from 'alchemy-sdk';

export default function GetTransactionDetails({tx, alchemy}){
    const [transactionDetails, setTransactionDetails] = useState();

    useEffect(() => {
        if(tx){
            async function getDetails() {
                setTransactionDetails(await alchemy.core.getTransactionReceipt(tx));
              }
            getDetails();
        }
    
      });

    return (
        <>
            {transactionDetails ? (
                    <>
                        <h3>Transaction Details:</h3>
                        <table>
                            <tr>
                                <td>
                                    To:
                                </td>
                                <td>
                                    {transactionDetails.to}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    From:
                                </td>
                                <td>
                                    {transactionDetails.from}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contract:
                                </td>
                                <td>
                                    {transactionDetails.contractAddress}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Transaction Index:
                                </td>
                                <td>
                                    {transactionDetails.transactionIndex}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Root:
                                </td>
                                <td>
                                    {transactionDetails.root}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gas Used:
                                </td>
                                <td>
                                    {Utils.formatEther(transactionDetails.gasUsed)} ETH
                                </td>
                            </tr>
                        </table>
                    </>
                )
                :
                    <>
                    </>    
            }
        </>
    )
}