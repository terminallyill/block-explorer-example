![Block Explorer Example](https://gitlab.com/terminallyill/block-explorer-example/-/raw/2af2de2b5bfa20164621049d7c0856ba4a1401b4/docs/blockexplorer.png)

# Ethereum Block Explorer
A hacky example of a Block Explorer for Ethereum. Built with Alchemy-SDK.

What it does:
- Live updates for latest block (async every 5 seconds)
- Search for specific block number
- Return block details
- View transaction details
