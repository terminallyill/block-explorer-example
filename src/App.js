import { Alchemy, Network } from 'alchemy-sdk';
import { useEffect, useState } from 'react';
import BlockSearch from './components/searchBar';
import GetLatestBlock from './components/latestBlock';
import GetBlockDetails from './components/blockDetails';
import GetTransactionDetails from './components/transactionDetails';

import './App.css';

const settings = {
  apiKey: process.env.REACT_APP_ALCHEMY_API_KEY,
  network: Network.ETH_MAINNET,
};

const alchemy = new Alchemy(settings);

function App() {
  const [latestBlock, setLatestBlock] = useState();
  const [searchBlock, setSearchBlock] = useState();
  const [detailBlock, setDetailBlock] = useState();
  const [selectedTransaction, setSelectedTransaction] = useState();

  return (
    <>
        <div className="block-identifier-container">
          <GetLatestBlock setLatestBlock={setLatestBlock} lb={latestBlock} setDetailBlock={setDetailBlock} db={detailBlock} alchemy={alchemy}/>
          <BlockSearch setSearchBlock={setSearchBlock} sb={searchBlock} setDetailBlock={setDetailBlock}/>
        </div>
        <div className="result-container">
          <GetBlockDetails db={detailBlock} alchemy={alchemy} setSelectedTransaction={setSelectedTransaction}/>
          <GetTransactionDetails tx={selectedTransaction} alchemy={alchemy}/>
        </div>
       </>
    )
}

export default App;
