import {useState, useEffect} from 'react';
import { Utils } from 'alchemy-sdk';
import SimpleBar from 'simplebar-react';
import 'simplebar-react/dist/simplebar.min.css';

export default function GetBlockDetails({db, alchemy, setSelectedTransaction}){

    const [blockDetails, setBlockDetails] = useState();

    useEffect(() => {
        if(db){
            async function getDetails() {
                setBlockDetails(await alchemy.core.getBlock(db));
              }
            getDetails();
        }
    
      });

    function getDetails(e){
        console.log("Selecting TX:", e.target.value);
        setSelectedTransaction(e.target.value);
    }

    return (
        <>
            {
                db ? (
                    <>
                        {
                            db && blockDetails ? (
                                <>  <div className="block-container">
                                        <div className="block-details-container">
                                            <h3>Details for Block: {db}</h3>
                                            <table>
                                                <tr>
                                                    <td>Detail</td><td>Value</td>
                                                </tr>
                                                <tr>
                                                    <td>Hash</td><td>{blockDetails.hash}</td>
                                                </tr>
                                                <tr>
                                                    <td>Parent Hash</td><td>{blockDetails.parentHash}</td>
                                                </tr>
                                                <tr>
                                                    <td>Number</td><td>{blockDetails.number}</td>
                                                </tr>
                                                <tr>
                                                    <td>Timestamp</td><td>{blockDetails.timestamp}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nonce</td><td>{blockDetails.nonce}</td>
                                                </tr>
                                                <tr>
                                                    <td>Difficulty</td><td>{blockDetails.difficulty}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gas Limit</td><td>{Utils.formatEther(blockDetails.gasLimit)} ETH</td>
                                                </tr>
                                                <tr>
                                                    <td>Gas Used</td><td>{Utils.formatEther(blockDetails.gasUsed)} ETH</td>
                                                </tr>
                                                <tr>
                                                    <td>Miner</td><td>{blockDetails.miner}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Transactions</td><td>{blockDetails.transactions.length}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div className="block-transactions-container">
                                            <h3>Transactions for Block: {db}</h3>
                                                {blockDetails.transactions.length > 0 ? (
                                                        <>
                                                            <tr style={{display:"grid",gridTemplateColumns:"30% 30% 30%"}}>
                                                                <td>Index</td>
                                                                <td>Hash</td>
                                                                <td>More Details</td>
                                                            </tr>
                                                            <SimpleBar style={{maxHeight: 300}}>
                                                                {blockDetails.transactions.map((tx, index) => {
                                                                    return (
                                                                        <tr key={index}>
                                                                            <td>{index}</td><td>{tx}</td><td><button value={tx} onClick={getDetails}>View Tx Details</button></td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            </SimpleBar>
                                                        </>
                                                    )
                                                    :
                                                        <>
                                                        <span>No transactions found!</span>
                                                        </>
                                                }
                                        </div>
                                    </div>
                                </>
                            )
                            :
                            <>
                                <span>Loading block details...</span>
                            </>
                        }
                    </>
                )
                :
                <>
                </>
            }
        </>
    )
}